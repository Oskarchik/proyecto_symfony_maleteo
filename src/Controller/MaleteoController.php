<?php


namespace App\Controller;


use App\Entity\Opinion;
use App\Entity\Solicitud;

use App\Entity\User;
use App\Form\OpinionType;
use App\Form\RegistroType;
use App\Form\SolicitudType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MaleteoController extends AbstractController
{
    /**
     * @Route("/maleteo", name="home_page")
     */
    public function homePage(Request $request, EntityManagerInterface $em)
    {
        $solicitud = new Solicitud();
        $formSolicitud = $this->createForm(SolicitudType::class, $solicitud);
        $formSolicitud->handleRequest($request);

        if( $formSolicitud->isSubmitted()&&$formSolicitud->isValid()) {
            $solicitud = $formSolicitud->getData();
            $em->persist($solicitud);
            $em->flush();
//            $this->addFlash('success', "Solicitud registrada correctamente");
            return $this->redirectToRoute('home_page');
        }
        return $this->render('index.html.twig', [
            'form' => $formSolicitud->createView()
        ]);

//        return $this->render('base.html.twig');
    }

    /**
     * @Route("/maleteo/solicitudes", name="requests")
     * @IsGranted("ROLE_ADMIN")
     */
    public function listRequests(EntityManagerInterface $em)
    {
        $solicitudes = $em->getRepository(Solicitud::class)->findAll();
        return $this->render('solicitudes.html.twig',
            [
                'solicitudes'=>$solicitudes
            ]);
    }

    /**
     * @Route("/maleteo/opiniones", name="opinions")
     */
    public function listOpinions(EntityManagerInterface $em)
    {
        $opinions = $em->getRepository(Opinion::class)->findAll();
        return $this->render('opinions.html.twig',
        [
            'opinions'=>$opinions
        ]);
    }


    /**
     * @Route("/maleteo/nueva-opinion", name="new_opinion")
     */
    public function newOpinion(Request $request, EntityManagerInterface $em)
    {
        $formOpinon = $this->createForm(OpinionType::class);
        $formOpinon->handleRequest($request);

        if ($formOpinon->isSubmitted() && $formOpinon->isValid()){
            $opinion = $formOpinon->getData();
            $em->persist($opinion);
            $em->flush();
            $this->addFlash('success', "Opinion registrada correctamente");
            return $this->redirectToRoute('new_opinion');
        }
        return $this->render('new-opinion.html.twig', [
            'opinion' => $formOpinon->createView()
        ]);
    }
    /**
     * @Route("/maleteo/registro", name="registro")
     */
    public function registrastion(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $formRegistro = $this->createForm(RegistroType::class);
        $formRegistro->handleRequest($request);

        if($formRegistro->isSubmitted() && $formRegistro->isValid()){
            $user = $formRegistro->getData();
            $user->setPassword($passwordEncoder->encodePassword($user, $formRegistro['password']->getData()));
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('home_page');
        }
        return $this->render('register.html.twig',
        [
            'registro'=> $formRegistro->createView()
        ]);
    }
}